import pytest

from my_math import (
    add,
    div,
    mod,
    mul,
    sub,
)


@pytest.mark.parametrize("a,b,expected", [(1, 2, 3), (4, -1, 3)])
def test_add(a: int, b: int, expected: int):
    assert add(a, b) == expected


@pytest.mark.parametrize("a,b,expected", [(3, 2, 1), (4, -1, 5)])
def test_sub(a: int, b: int, expected: int):
    assert sub(a, b) == expected


@pytest.mark.parametrize("a,b,expected", [(1, 2, 2), (4, -1, -4), (2, 0, 0)])
def test_mul(a: int, b: int, expected: int):
    assert mul(a, b) == expected


@pytest.mark.parametrize("a,b,expected", [(6, 4, 1.5), (2, 1, 2)])
def test_div(a: int, b: int, expected: float):
    assert div(a, b) == expected


def test_div_by_zero():
    with pytest.raises(ZeroDivisionError):
        div(2, 0)


@pytest.mark.parametrize("a,b,expected", [(6, 4, 2), (2, 2, 0)])
def test_mod(a: int, b: int, expected: int):
    assert mod(a, b) == expected
