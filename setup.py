import setuptools

setuptools.setup(
    name="mymath",
    version="0.1.0",
    author="gonczor",
    author_email="",
    description="A small example package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)

